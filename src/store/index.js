import Vue from 'vue'
import Vuex from 'vuex'

import datos from '../../public/posts_data.json'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    results: datos,
    element: ''
  },
  getters: {
    // CARGAMOS LOS POST
    loadPost (state) {
      return state.results.map((item) => {
        return item
      })
    }
  },
  mutations: {
    // CARGAMOS LO ENCONTRADO EN EL ARRAY DE RESULTADOS
    // search (state) {
    //   state.results = [state.element, ...state.results]
    // }
  },
  actions: {
    // aplyFilter (context) {
    //   context.commit('search')
    // }
  },
  modules: {
  }
})
