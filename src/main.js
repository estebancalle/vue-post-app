import 'bootstrap/dist/css/bootstrap.css'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import * as bootstrap from 'bootstrap'
import 'bootstrap/dist/js/bootstrap.js'

Vue.config.productionTip = false
Vue.use(bootstrap)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
