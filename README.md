
## VUE POST APP FOR HORIZM


Desarrollo de una app que consume un json estático 
para listar, diagramar y manejar algunas propiedades
de la mano con vue


## Instalación

Luego de haber clonado ejecuta el siguiente comando de node

```bash
  $ npm install
```

Luego

```bash
  $ npm run serve
```


## Autores

- [@estebancalle](https://gitlab.com/estebancalle)

